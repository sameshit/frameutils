#ifndef VideoConverterTest__HH
#define VideoConverterTest__HH

#include "../src/FrameUtils.h"

namespace FrameUtilsLib
{
	class VideoConverterTest
	{
	public:
		VideoConverterTest(CoreObjectLib::CoreObject *core);
		virtual ~VideoConverterTest();

		bool Run();
	private:
		CoreObjectLib::CoreObject *_core;
		I420Converter *_converter;
	};
}

#endif