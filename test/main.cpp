#include "VideoConverterTest.h"

using namespace CoreObjectLib;
using namespace FrameUtilsLib;
using namespace std;


int main()
{
	CoreObject *_core = new CoreObject;
	VideoConverterTest *test;

	fast_new(test,_core);
	ABORT_IF_FALSE(test->Run());
	cin.get();
	fast_delete(test);

	delete _core;
	return 0;
}