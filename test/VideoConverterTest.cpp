#include "VideoConverterTest.h"

using namespace CoreObjectLib;
using namespace FrameUtilsLib;
using namespace std;

VideoConverterTest::VideoConverterTest(CoreObject *core)
	:_core(core)
{
	fast_new(_converter);
}

VideoConverterTest::~VideoConverterTest()
{
	fast_delete(_converter);
}

const size_t kTestCount = 5000;

bool VideoConverterTest::Run()
{
	uint8_t *data1,*data2;
	uint32_t width1,height1,width2,height2,size1,size2;
	COTime begin,end;

	width1 = 1440;
	height1 = 900;

	width2 = 1440;
	height2 = 900;

	size1 = width1*height1*3/2;
	size2 = width2*height2*3;
	fast_aligned(data1,16,size1);
	fast_aligned(data2,16,size2);
	for (uint32_t i = 0 ; i < size1; ++i)
		data1[i] = i;


	RETURN_IF_FALSE(_converter->Open(PixelFormat::BGR888,width1,height1,width2,height2));

	begin.Update();
	for (auto i = 0; i < kTestCount; ++i)
		_converter->Convert(data2,data1);
	end.Update();
	LOG_INFO("RGB888->I420 time: "<<end-begin<<", fps: "<<kTestCount*1000/(end-begin));


	fast_free(data1);
	fast_free(data2);
	return true;
}