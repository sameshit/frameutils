#ifndef I420CONVERTER__H
#define I420CONVERTER__H

#include "VideoFrameTypes.h"
#include "../../libyuv/source/libyuv.h"

namespace FrameUtilsLib
{
	class I420Converter
	{
	public:
		I420Converter();
		virtual ~I420Converter();

		bool Open(const PixelFormat &pf,const uint32_t &input_width, const uint32_t &input_height,
				  const uint32_t &output_width,const uint32_t &output_height);
		inline void Convert(const uint8_t *input, uint8_t *output,size_t size = 0)
		{
			libyuv::ConvertToI420(input,size,output,0,output+_input_width*_input_height,0,
				output+5*_input_width*_input_height/4,0,0,0,_input_width,_input_height,_output_width,_output_height,
				libyuv::kRotate0,_format);
		}
	private:
		uint32_t _input_width,_input_height,_output_width,_output_height,_format;
	};
}



#endif