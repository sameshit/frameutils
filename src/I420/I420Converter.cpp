#include "I420Converter.h"

using namespace FrameUtilsLib;
using namespace CoreObjectLib;
using namespace libyuv;

I420Converter::I420Converter()
{

}

I420Converter::~I420Converter()
{

}

bool I420Converter::Open(const PixelFormat &pf,const uint32_t &input_width, const uint32_t &input_height,
				  const uint32_t &output_width,const uint32_t &output_height)
{
	switch (pf)
	{
	case PixelFormat::ARGB:
		_format = libyuv::FOURCC_ARGB;
	break;
	case PixelFormat::I420:
		_format = libyuv::FOURCC_I420;
	break;
	case PixelFormat::BGRA:
		_format = libyuv::FOURCC_BGRA;
	break;
	case PixelFormat::I400:
		_format = libyuv::FOURCC_I400;
	break;
	case PixelFormat::I411:
		_format = libyuv::FOURCC_I411;
	break;
	case PixelFormat::I422:
		_format = libyuv::FOURCC_I422;
	break;
	case PixelFormat::I444:
		_format = libyuv::FOURCC_I444;
	break;
	case PixelFormat::JPEG:
		_format = libyuv::FOURCC_MJPG;
	break;
	case PixelFormat::NV12:
		_format = libyuv::FOURCC_NV12;
	break;
	case PixelFormat::NV21:
		_format = libyuv::FOURCC_NV21;
	break;
	case PixelFormat::RGB4444:
		_format = libyuv::FOURCC_R444;
	break;
	case PixelFormat::RGB555:
		_format = libyuv::FOURCC_RGBO;
	break;
	case PixelFormat::RGB565:
		_format = libyuv::FOURCC_RGBP;
	break;
	case PixelFormat::BGR888:
		_format = libyuv::FOURCC_RGB3;
	break;
	case PixelFormat::RGBA:
		_format = libyuv::FOURCC_RGBA;
	break;
	case PixelFormat::UYVY:
		_format = libyuv::FOURCC_UYVY;
	break;
	case PixelFormat::YUY2:
		_format = libyuv::FOURCC_YUY2;
	break;
	case PixelFormat::YV12:
		_format = libyuv::FOURCC_YV12;
	break;
	case PixelFormat::YV16:
		_format = libyuv::FOURCC_YV16;
	break;
	case PixelFormat::YV24:
		_format = libyuv::FOURCC_YV24;
	break;
	default:
		COErr::Set() << "This pixel format is not supported";
		return false;
	break;
	}

	_input_height = input_height;
	_input_width  = input_width;
	_output_height= output_height;
	_output_width = output_width;
	return true;
}