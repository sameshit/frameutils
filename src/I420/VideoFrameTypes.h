#ifndef VIDEOFRAMETYPES___H
#define VIDEOFRAMETYPES___H

#include "../../../CoreObject/src/CoreObject.h"

namespace FrameUtilsLib
{
	enum class PixelFormat
	{
		Unknown,
		YUY2,
		I420,
		I422,
		I444,
		I411,
		I400,
		NV21,
		NV12,
		UYVY,
		VDA,
		RGB565,
		RGB555,
		BGR888,
		RGB4444,
		ARGB,
		RGBA,
		BGRA,
		JPEG,
		YV12,
		YV16,
		YV24,
	};
}

#endif