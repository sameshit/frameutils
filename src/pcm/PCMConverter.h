#ifndef PCMCONVERTER__H
#define PCMCONVERTER__H

#include "AudioFrameTypes.h"

namespace FrameUtilsLib
{
	class PCMConverter
	{
	public:
		PCMConverter();
		virtual ~PCMConverter();

		bool Open(const uint16_t &channels);
		inline void Convert(const uint8_t *float_input, uint8_t *pcm_output,const size_t& samples)
		{
			size_t i;
			int16_t *ptr ;
			float mono,*f_input;

			f_input = (float *)float_input;
			ptr = (int16_t*)pcm_output;
			for(i = 0 ; i < _channels*samples ; i++, ++ptr)
			{
				mono = f_input[i] ;
				if (mono < -1.0)
					mono = -1.0;
				else if (mono > 1.0)
					mono = 1.0;
				*ptr = (int16_t)(mono * 32767.f);
			}
		}
	private:
		uint16_t _channels;
	};
}

#endif