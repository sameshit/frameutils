#ifndef VIDEOFRAMETYPES___H
#define VIDEOFRAMETYPES___H

#include "../../../CoreObject/src/CoreObject.h"

namespace FrameUtilsLib
{
	enum class AudioSampleFormat
	{
		Unknown,
		PCM,
		Float,
	};
}

#endif